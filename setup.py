from distutils.core import setup

setup(
    name='testify',
    version='1.0.0',
    packages=[''],
    url='https://gitlab.com/brendan-donegan/testify',
    license='LGPL',
    author='brendand',
    author_email='brendan.j.donegan@gmail.com',
    description='Test Management System'
)
