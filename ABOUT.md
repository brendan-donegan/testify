Testify
=======

A common problem when manually testing software projects is that existing test
management systems are costly, and teams like to fool themselves into believing
they'll only do 'limited' manual testing and it will be manageable. Then what
inevitably happens is that test cases are shoveled into some kind of document.
Eventually the realisation will sink in that a test case management system is
required, by which time the team has to invest a lot of time in bootstrapping
it with their existing test cases.

Testify's goal is to nip this on the bud by providing a free and open source
alternative.